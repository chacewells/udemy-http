// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
  // , firebase: {
  //   apiKey: 'AIzaSyAJccVny0BhAsHpIFWqoQ7X0RI6U3MF2PA',
  //   authDomain: 'udemy-ng-http-b3bb5.firebaseapp.com',
  //   databaseURL: 'https://udemy-ng-http-b3bb5.firebaseio.com',
  //   projectId: 'udemy-ng-http-b3bb5',
  //   storageBucket: 'udemy-ng-http-b3bb5.appspot.com',
  //   messagingSenderId: '1002774284350'
  // }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
