import {Injectable} from '@angular/core';
import {Http, RequestMethod, RequestOptions, Headers, Response} from '@angular/http';
import {HttpHeaders} from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class ServerService {
  constructor(private http: Http) {}

  storeServers(servers: any[]) {
    const headers = new Headers({'Content-Type': 'application/json'});
    // return this.http.post(
    return this.http.put(
      'http://localhost:3000/servers',
      servers,
      {headers: headers});
  }

  getServers() {
  return this.http.get('http://localhost:3000/servers')
    .pipe(
      map((response: Response) => {
        const data = response.json();
        for (const server of data) {
          server.name = 'FECTHED_' + server.name;
        }
        return data;
      },
      catchError((error: Response) => throwError('something went wrong'))
      )
    );
  }

  getAppName() {
    return this.http.get('http://localhost:3000/appName')
      .pipe(
        map((response: Response) => response.json().appName)
      );
  }
}
